#! /bin/sh

# spec file runs make check or whatever
# which contains some functionality check.
# So We don't do detailed check here.
if fribidi --help > /dev/null 2>&1; then
	echo PASS
else
	echo FAIL
	false
fi

